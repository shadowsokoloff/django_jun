from django.contrib import admin
from .models import Sith, Planet, Test, Question

admin.site.register(Question)
admin.site.register(Sith)
admin.site.register(Test)
admin.site.register(Planet)