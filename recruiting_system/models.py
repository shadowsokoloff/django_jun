from django.db import models
from django.forms import ModelForm
from django import forms 

class Test(models.Model):
    orden_id = models.PositiveIntegerField()


class Question(models.Model):
    question_text = models.TextField()
    test = models.ForeignKey(Test, on_delete=models.CASCADE)

    def __str__(self):
        return self.question_text


class Planet(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Sith(models.Model):
    name = models.CharField(max_length=100)
    planet = models.ForeignKey(Planet,on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.name


class Recruit(models.Model):
    recruit_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    planet = models.ForeignKey(Planet, on_delete=models.CASCADE) 
    age = models.PositiveIntegerField(default=0)
    email = models.EmailField(unique=True)
    sith = models.ForeignKey(Sith, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.name


class Answer(models.Model):
    answer_text = models.TextField()
    question = models.ForeignKey(Question, on_delete=models.CASCADE)  
    recruit = models.ForeignKey(Recruit, on_delete=models.CASCADE)
    question_text = models.TextField(blank=True)

    def __str__(self):
        return self.answer_text




