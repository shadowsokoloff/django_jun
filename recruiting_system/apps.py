from django.apps import AppConfig


class RecruitingSystemConfig(AppConfig):
    name = 'recruiting_system'
