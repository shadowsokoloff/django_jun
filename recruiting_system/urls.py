from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('siths/', views.siths, name='siths'),
    path('recruit/', views.recruit, name='recruit'),
    path('test/<int:recruit_id>/', views.test, name='test'),
    path('recruits_list/<int:sith_id>/', views.recruits_list, name='recruits_list'),
    path('recruits_list/<int:sith_id>/recruit_profile/<int:recruit_id>/', views.recruit_profile, name='recruit_profile'),
    path('recruits_list/<int:sith_id>/recruit_profile/<int:recruit_id>/add', views.add_recruit, name='add_recruit'),
    path('sith/<int:sith_id>/', views.sith, name='sith'),
    path('siths_recruits/<int:sith_id>/', views.siths_recruits, name='siths_recruits'),
]