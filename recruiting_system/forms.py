from .models import *


class AnswerForm(forms.Form):
    recruit_id = forms.CharField(widget=forms.HiddenInput())
    question_id = forms.CharField(widget=forms.HiddenInput())
    question_text = forms.CharField(widget=forms.HiddenInput())
    answer_text = forms.CharField()
    
    def get_question_text(self):
        return self.question_text


class RecruitForm(ModelForm):
    class Meta:
        model = Recruit
        exclude = ['sith']

    def clean_id(self):
        id = self.cleaned_data.get('recruit_id')
        return id

    def clean_email(self):
        email = self.cleaned_data.get('email')

        try:
            match = Recruit.objects.get(email=email)
        except Recruit.DoesNotExist:
            return email
        raise forms.ValidationError('Already existing email!')