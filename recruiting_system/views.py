from django.shortcuts import render, redirect
from recruiting_system.models import *
from django.http import HttpResponse 
from django import forms
from django.shortcuts import get_object_or_404
from django.forms import formset_factory, modelformset_factory
from .forms import *
from django.core.mail import send_mail



def index(request):
    return render(request, 'recruiting_system/index.html')

def siths(request):
    siths_list = Sith.objects.all()
    context = {'siths_list': siths_list}
    return render(request, 'recruiting_system/siths.html', context)

def recruit(request):
    if request.method == 'POST':
        form = RecruitForm(request.POST)
        if form.is_valid():
            context = {
                'recruit_email': form.cleaned_data['email'],
                'question_list': Question.objects.all()
            }
            instance = form.save()
            id = instance.pk
            return redirect('test', recruit_id=id)
        else:
            return HttpResponse(f"<h1>{form.errors['email']}</h1>")
    else:
        form = RecruitForm()
        return render(request, 'recruiting_system/recruit.html', {'form': form})   


def test(request, recruit_id):
    question_list = Question.objects.all()
    qlist_len = len(question_list)
    recruit = get_object_or_404(Recruit, pk=recruit_id)
    num_answer = Answer.objects.filter(recruit=recruit).count()
    idx = 0
    data = []
    for question in question_list:
        data_dict = {f"recruit_id": int(recruit_id),
                f"question_id": int(question.id),
                f"question_text": question.question_text,
                }
        data.append(data_dict)
        idx += 1
    AnswerFormSet = formset_factory(AnswerForm, max_num=qlist_len)
    if request.method == 'POST' and not num_answer:
        answer_list = AnswerFormSet(request.POST, initial=data)
        if answer_list.is_valid():
            for answerform in answer_list:
                recruit = Recruit.objects.get(pk=answerform.cleaned_data['recruit_id'])
                question = Question.objects.get(pk=answerform.cleaned_data['question_id'])
                question_text = answerform.cleaned_data['question_text']
                answer_text = answerform.cleaned_data['answer_text']
                answer = Answer(question=question, answer_text=answer_text, recruit=recruit, question_text=question_text)
                #print(answer)
                answer.save()
        return HttpResponse('cool')
    elif num_answer != 0:
        return HttpResponse('You have already answered the question')
    else:
        answer_list = AnswerFormSet(initial=data)
        for answer in answer_list:
            answer.fields['answer_text'].label = answer.initial['question_text']
        return render(request, 'recruiting_system/test.html', {'answer_list': answer_list})
    

def recruits_list(request, sith_id):
    sith = get_object_or_404(Sith, pk=sith_id)
    recruits_list = Recruit.objects.exclude(sith=sith)
    sith_id = sith_id
    if request.method == 'GET':
        return render(request, 'recruiting_system/recruits_list.html', {'recruits_list': recruits_list, 'sith_id': sith_id})


def recruit_profile(request, recruit_id, sith_id):
    recruit = get_object_or_404(Recruit, pk=recruit_id)
    recruit_id = recruit_id
    if request.method == 'GET':
        answer_list = Answer.objects.filter(recruit=recruit)
        recruit = recruit.name
        return render(request, 'recruiting_system/recruit_profile.html', {'answer_list': answer_list, 'recruit_id': recruit_id, 'sith_id': sith_id, 'recruit': recruit})


def add_recruit(request, sith_id, recruit_id):
    sith = get_object_or_404(Sith, pk=sith_id)
    recruit = get_object_or_404(Recruit, pk=recruit_id)
    email = []
    email.append(recruit.email)
    if request.method == 'GET':
        recruit.sith = sith
        recruit.save()
        send_mail('You WIN!', f'Congratulations! The sith {sith.name} chose you', 'shadowsokoloff@yahoo.com', email)
        return HttpResponse('cool')


def sith(request, sith_id):
    sith = get_object_or_404(Sith, pk=sith_id)
    if request.method == 'GET':
        return render(request, 'recruiting_system/sith.html', {'sith': sith})


def siths_recruits(request, sith_id):
    sith = get_object_or_404(Sith, pk=sith_id)
    recruits_list = Recruit.objects.filter(sith=sith)
    if request.method == 'GET':
        return render(request, 'recruiting_system/siths_recruits.html', {'recruits_list': recruits_list})